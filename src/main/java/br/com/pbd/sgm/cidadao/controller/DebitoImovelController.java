package br.com.pbd.sgm.cidadao.controller;

import br.com.pbd.sgm.cidadao.controller.dto.DebitoImovelDTO;
import br.com.pbd.sgm.cidadao.service.DebitoImovelService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/debitos-imoveis")
public class DebitoImovelController {

    private final DebitoImovelService service;

    public DebitoImovelController(DebitoImovelService service) {
        this.service = service;
    }

    @GetMapping("/{inscricao}")
    public List<DebitoImovelDTO> buscarDebitosPorInscricaoImovel(@PathVariable String inscricao){
        return service.buscarPorInscricaoImovel(inscricao);
    }
}

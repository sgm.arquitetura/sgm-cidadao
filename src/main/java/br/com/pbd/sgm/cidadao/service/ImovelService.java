package br.com.pbd.sgm.cidadao.service;

import br.com.pbd.sgm.cidadao.client.IdentidadeClient;
import br.com.pbd.sgm.cidadao.client.IntegracaoSturClient;
import br.com.pbd.sgm.cidadao.client.dto.ImovelSturDTO;
import br.com.pbd.sgm.cidadao.client.dto.UsuarioDTO;
import br.com.pbd.sgm.cidadao.controller.dto.ImovelDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ImovelService {

    private final IntegracaoSturClient integracaoSturClient;
    private final IdentidadeClient identidadeClient;

    public ImovelService(IntegracaoSturClient integracaoSturClient, IdentidadeClient identidadeClient) {
        this.integracaoSturClient = integracaoSturClient;
        this.identidadeClient = identidadeClient;
    }

    public List<ImovelDTO> buscarImoveisDoUsuarioAutenticado() {
        UsuarioDTO usuarioAutenticado = identidadeClient.buscarAutenticado();
        List<ImovelSturDTO> imoveis = integracaoSturClient.buscarImoveisPorCpf(usuarioAutenticado.getCpf());
        return imoveis.stream().map(ImovelDTO::new).collect(Collectors.toList());
    }

    public ImovelDTO buscarPorInscricao(String inscricao) {
        UsuarioDTO usuarioAutenticado = identidadeClient.buscarAutenticado();
        List<ImovelSturDTO> imoveis = integracaoSturClient.buscarImoveisPorCpf(usuarioAutenticado.getCpf());
        Optional<ImovelSturDTO> optinal = imoveis.stream().filter(imovel -> imovel.getNumeroInscricao().equals(inscricao)).findFirst();
        if(optinal.isPresent()) { return new ImovelDTO(optinal.get()); }
        throw new RuntimeException("Imóvel não encontrado");
    }
}

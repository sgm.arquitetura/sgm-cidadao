package br.com.pbd.sgm.cidadao.controller.dto;

import br.com.pbd.sgm.cidadao.client.dto.DebitoImovelSturDTO;

import static java.util.Optional.ofNullable;

public class DebitoImovelDTO {

    private String id;
    private String inscricaoImovel;
    private String tipoDebito;
    private String ano;
    private String valor;
    private String multa;
    private String juros;
    private String outros;
    private SituacaoDebito situacao;

    public DebitoImovelDTO(DebitoImovelSturDTO debito) {
        this.id = debito.getId();
        this.inscricaoImovel = debito.getInscricaoImovel();
        this.tipoDebito =debito.getTipoDebito();
        this.ano = debito.getAno();
        ofNullable(debito.getValor()).ifPresent(v -> this.valor = v.replace(".",","));
        ofNullable(debito.getMulta()).ifPresent(v-> this.multa = v.replace(".",","));
        ofNullable(debito.getJuros()).ifPresent(v-> this.juros = v.replace(".",","));
        ofNullable(debito.getOutros()).ifPresent(v-> this.outros = v.replace(".",","));
        this.situacao = SituacaoDebito.findByName(debito.getSituacao());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInscricaoImovel() {
        return inscricaoImovel;
    }

    public void setInscricaoImovel(String inscricaoImovel) {
        this.inscricaoImovel = inscricaoImovel;
    }

    public String getTipoDebito() {
        return tipoDebito;
    }

    public void setTipoDebito(String tipoDebito) {
        this.tipoDebito = tipoDebito;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getMulta() {
        return multa;
    }

    public void setMulta(String multa) {
        this.multa = multa;
    }

    public String getJuros() {
        return juros;
    }

    public void setJuros(String juros) {
        this.juros = juros;
    }

    public String getOutros() {
        return outros;
    }

    public void setOutros(String outros) {
        this.outros = outros;
    }

    public String getSituacao() {
        return situacao.toString();
    }

    public void setSituacao(SituacaoDebito situacao) {
        this.situacao = situacao;
    }
}

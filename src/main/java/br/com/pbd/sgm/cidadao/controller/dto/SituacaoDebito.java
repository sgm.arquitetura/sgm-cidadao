package br.com.pbd.sgm.cidadao.controller.dto;

import java.util.Arrays;

public enum SituacaoDebito {
    PENDENTE_PAGAMENTO( "Pendente de pagamento"),
    DIVIDA_ATIVA("Dívida Ativa"),
    PAGO("Pago");

    private final String descricao;

    SituacaoDebito(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

    static SituacaoDebito findByName(String name) {
        return Arrays.stream(SituacaoDebito.values())
                .filter(s -> s.name().equals(name))
                .findFirst().orElse(null);
    }
}

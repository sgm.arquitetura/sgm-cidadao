package br.com.pbd.sgm.cidadao.controller.dto;

import br.com.pbd.sgm.cidadao.client.dto.ImovelSturDTO;

import static org.apache.commons.lang.StringUtils.isNotBlank;

public class ImovelDTO {

    private String inscricao;
    private String endereco;
    private String bairro;
    private String cep;
    private String localidade;
    private String area;

    public ImovelDTO(ImovelSturDTO imovelStur) {
        this.inscricao = imovelStur.getNumeroInscricao();
        this.endereco = imovelStur.getEndereco();
        this.bairro = imovelStur.getBairro();
        this.cep = imovelStur.getCep();
        this.localidade = formatarLocalidade(imovelStur);
        this.area = formatarArea(imovelStur.getArea());
    }


    public String getInscricao() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    private String formatarArea(String area) {
        if(isNotBlank((area))) { return area.replace(".",","); }
        return area;
    }

    private String formatarLocalidade(ImovelSturDTO imovelStur) {
        return String.format("%s / %s", imovelStur.getCidade(), imovelStur.getUf());
    }
}

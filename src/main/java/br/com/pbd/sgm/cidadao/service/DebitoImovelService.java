package br.com.pbd.sgm.cidadao.service;

import br.com.pbd.sgm.cidadao.client.IdentidadeClient;
import br.com.pbd.sgm.cidadao.client.IntegracaoSturClient;
import br.com.pbd.sgm.cidadao.client.dto.DebitoImovelSturDTO;
import br.com.pbd.sgm.cidadao.client.dto.UsuarioDTO;
import br.com.pbd.sgm.cidadao.controller.dto.DebitoImovelDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DebitoImovelService {

    private final IntegracaoSturClient integracaoSturClient;
    private final IdentidadeClient identidadeClient;

    public DebitoImovelService(IntegracaoSturClient integracaoSturClient, IdentidadeClient identidadeClient) {
        this.integracaoSturClient = integracaoSturClient;
        this.identidadeClient = identidadeClient;
    }

    public List<DebitoImovelDTO> buscarPorInscricaoImovel(String inscricaoImovel) {
        UsuarioDTO usuarioAutenticado = identidadeClient.buscarAutenticado();
        List<DebitoImovelSturDTO> debitos = integracaoSturClient.buscarDebitosPorInscricaoImovel(inscricaoImovel);
        return debitos.stream().map(DebitoImovelDTO::new).collect(Collectors.toList());
    }
}

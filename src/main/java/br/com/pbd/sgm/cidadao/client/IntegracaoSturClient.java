package br.com.pbd.sgm.cidadao.client;

import br.com.pbd.sgm.cidadao.client.dto.DebitoImovelSturDTO;
import br.com.pbd.sgm.cidadao.client.dto.ImovelSturDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("integracao")
public interface IntegracaoSturClient {

    @GetMapping("/stur/imoveis/{cpf}")
    List<ImovelSturDTO> buscarImoveisPorCpf(@PathVariable String cpf);

    @GetMapping("/stur/debitos/{inscricaoImovel}")
    List<DebitoImovelSturDTO> buscarDebitosPorInscricaoImovel(@PathVariable String inscricaoImovel);

}

package br.com.pbd.sgm.cidadao.client;

import br.com.pbd.sgm.cidadao.client.dto.UsuarioDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("identity")
public interface IdentidadeClient {

    @GetMapping("/users/auth")
    UsuarioDTO buscarAutenticado();

}

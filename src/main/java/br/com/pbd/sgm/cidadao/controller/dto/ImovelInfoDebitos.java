package br.com.pbd.sgm.cidadao.controller.dto;

import java.util.List;

public class ImovelInfoDebitos {

    private String inscricao;
    private String proprietario;
    private String endereco;

    public ImovelInfoDebitos(String inscricao) {
        this.inscricao = inscricao;
    }

    private List<DebitoImovelDTO> debitos;

    public String getInscricao() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public List<DebitoImovelDTO> getDebitos() {
        return debitos;
    }

    public void setDebitos(List<DebitoImovelDTO> debitos) {
        this.debitos = debitos;
    }
}

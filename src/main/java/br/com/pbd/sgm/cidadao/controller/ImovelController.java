package br.com.pbd.sgm.cidadao.controller;

import br.com.pbd.sgm.cidadao.controller.dto.ImovelDTO;
import br.com.pbd.sgm.cidadao.service.ImovelService;
import org.hibernate.mapping.Collection;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("imoveis")
public class ImovelController {

    private final ImovelService imovelService;

    public ImovelController(ImovelService imovelService) {
        this.imovelService = imovelService;
    }

    @GetMapping
    public List<ImovelDTO> buscarImoveisDoUsuarioAutenticado() {
        return imovelService.buscarImoveisDoUsuarioAutenticado();
    }

    @GetMapping("/{inscricao}")
    public ImovelDTO buscarImovelPorInscricao(@PathVariable String inscricao) {
        return imovelService.buscarPorInscricao(inscricao);
    }
}

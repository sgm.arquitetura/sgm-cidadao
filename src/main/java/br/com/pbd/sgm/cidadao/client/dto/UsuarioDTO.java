package br.com.pbd.sgm.cidadao.client.dto;

public class UsuarioDTO {

    private Long id;
    private String nome;
    private String cpf;
    private String email;
    private boolean ativo;
    private String[] perfis;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String[] getPerfis() {
        return perfis;
    }

    public void setPerfis(String[] perfis) {
        this.perfis = perfis;
    }
}
